//
//  HomeScreenController.swift
//  twitter
//
//  Created by CL-Macmini143 on 2/29/16.
//  Copyright © 2016 CL-Macmini143. All rights reserved.
//

import UIKit

class HomeScreenController: UITableViewController,UITextFieldDelegate
{
    // VARIABLES
    var defaults = NSUserDefaults.standardUserDefaults()
    let alertController : UIAlertController = UIAlertController()
    let textView : UITextView = UITextView()
    var tweeting : NSArray!
    var tweetid : NSArray!
    var names : NSArray!
    var nolikes : NSArray!
    var refreshAlert = UIAlertController()
  
    
    //OUTLETS
    @IBOutlet var noOfLikes: UILabel!
    @IBOutlet var menuButton: UIBarButtonItem!
    
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewDidAppear(animated: Bool)
    {
        tableView.reloadData()
         if Reachability.isConnectedToNetwork() == true
         {
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl!.addTarget(self, action: "onButton:", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(self.refreshControl!)
        }
        else
        {
            let refreshAlert = UIAlertController(title: "Internet not available", message: "Try Again Later", preferredStyle: UIAlertControllerStyle.Alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(refreshAlert, animated: false, completion: nil)
            
        }
        
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableViewAutomaticDimension
        _ = defaults.objectForKey("authidentity") as! NSString
        names = defaults.objectForKey("users") as! NSArray
        tweeting = defaults.objectForKey("tweets") as! NSArray
        tweetid = defaults.objectForKey("IdOfTweets") as! NSArray
        nolikes = defaults.objectForKey("Like") as! NSArray
        if self.revealViewController() != nil
        {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.revealViewController().rearViewRevealWidth = view.frame.size.width/2.5
        tableView.reloadData()
    }
    
   // send server call to display tweets api
 
    @IBAction func onButton(sender: AnyObject)
    {
        self.showTweets()        
    }
    
    // SERVER CALL TO LIKE TWEETS
    @IBAction func onLike(sender: AnyObject)
    {
        if Reachability.isConnectedToNetwork() == true {
        let x=sender.tag
        let auth = defaults.objectForKey("authidentity") as! NSString
        let url:NSURL = NSURL(string: "http://54.173.40.155:3005/api/v1/Tweet/like_unlike")!
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "PUT"
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        let parameters = [ "tweetId": tweetid[x] as! String] as Dictionary<String, String>
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        let boundaryConstant = "myRandomBoundary12345"
        let contentType = "application/json;boundary="+boundaryConstant
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        request.setValue( "\(auth)", forHTTPHeaderField: "auth")
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if let httpResponse = response as? NSHTTPURLResponse
            {
                if(httpResponse.statusCode == 200)
                {
                    dispatch_async(dispatch_get_main_queue())
                        {
                    self.showTweets()
                        }
                }
                else
                {
                    let refreshAlert = UIAlertController(title: "SORRY", message: "Like Option Is Currently Not Available", preferredStyle: UIAlertControllerStyle.Alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                    self.presentViewController(refreshAlert, animated: false, completion: nil)
                }
            }
        })
        task.resume()
        }
        else
        {
            let refreshAlert = UIAlertController(title: "Internet Connection Failed", message: "Try Again Later", preferredStyle: UIAlertControllerStyle.Alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(refreshAlert, animated: false, completion: nil)
        }
    }
    
    
    // UNWIND SEGUE DECLARATION
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {}
    
   // FUNCTION TO SHOW TWEETS
    func showTweets()
    {
        if Reachability.isConnectedToNetwork() == true {
        let auth = self.defaults.objectForKey("authidentity") as! NSString
        let url:NSURL = NSURL(string: "http://54.173.40.155:3005/api/v1/Tweet/Display_Tweets/desending")!
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        let boundaryConstant = "myRandomBoundary12345"
        let contentType = "application/json;boundary="+boundaryConstant
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        request.setValue( "\(auth)", forHTTPHeaderField: "auth")
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
        if let httpResponse = response as? NSHTTPURLResponse
        {
           if(httpResponse.statusCode == 200)
                {

                    self.parsingData(data!)
                    self.names = self.defaults.objectForKey("users") as! NSArray
                    self.tweeting = self.defaults.objectForKey("tweets") as! NSArray
                    self.tweetid = self.defaults.objectForKey("IdOfTweets") as! NSArray
                    self.nolikes = self.defaults.objectForKey("Like") as! NSArray!
                    dispatch_async(dispatch_get_main_queue())
                        {
                    self.tableView.reloadData()
                        }
                    
    
                }
        else
            {
            dispatch_async(dispatch_get_main_queue())
                    {
            self.refreshAlert = UIAlertController(title: "Sorry", message: "Try Again Later", preferredStyle: UIAlertControllerStyle.Alert)
            self.refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(self.refreshAlert, animated: false, completion: nil)
                    }
            }
           }
        })
        task.resume()
        self.refreshControl!.endRefreshing()
        }
        else
        {
            self.refreshAlert = UIAlertController(title: "Internet Connection Failed", message: "Try Again Later", preferredStyle: UIAlertControllerStyle.Alert)
            self.refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(self.refreshAlert, animated: false, completion: nil)
        }
        }

    
    // FUNCTION PARSING JSON RESPONSE
    func parsingData(data:NSData)
    {
        let dict: NSDictionary!=(try! NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
        let  temp = dict.valueForKey("data") as! NSDictionary
        let temp1 = temp.valueForKey("tweets") as! NSArray
        let tweets = temp1.valueForKey("tweet") as! NSArray
        let tweetid = temp1.valueForKey("_id") as! NSArray
        let like1 = temp1.valueForKey("likes") as! NSArray
        let usernames = temp1.valueForKey("id") as! NSArray
        let users = usernames.valueForKey("userName") as! NSArray
        self.defaults.setObject(like1, forKey: "Like")
        self.defaults.setObject(tweetid, forKey: "IdOfTweets")
        self.defaults.setObject(tweets, forKey: "tweets")
        self.defaults.setObject(users, forKey: "users")
    }

   // NUMBER OF ROWS OF TABLE
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tweeting.count
    }
    
   // LOADING CELLS OF TABLE
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    
    {
        let cell:addPersonCell = tableView.dequeueReusableCellWithIdentifier("cell") as! addPersonCell
        cell.tweet.text=tweeting[indexPath.row] as? String
        cell.username.text=names[indexPath.row] as? String
        cell.noOfLikes.text=String(nolikes[indexPath.row].count)
        cell.like.tag=indexPath.row
        return cell
    }
    
    
}
