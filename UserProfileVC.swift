//
//  notificationController.swift
//  twitter
//
//  Created by CL-Macmini143 on 3/1/16.
//  Copyright © 2016 CL-Macmini143. All rights reserved.
//

import UIKit

class notificationController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    var defaults = NSUserDefaults.standardUserDefaults()

    // OUTLETS
    @IBOutlet var textView: UITextView!
    @IBOutlet var name: UILabel!
    @IBOutlet var number: UILabel!
    @IBOutlet var email: UILabel!
    @IBOutlet var followers: UILabel!
    @IBOutlet var following: UILabel!
  
 
    override func viewWillAppear(animated: Bool)
    {
    self.navigationController?.navigationBarHidden = true
    }
    
    override func viewDidLoad()
    {
    super.viewDidLoad()
    }
    
    // UNWIND TO HOME SCREEN
    @IBAction func onBack(sender: AnyObject)
    {
    self.performSegueWithIdentifier("unwindToMenuController", sender: self)
    }
    
   // ON SEARCH BUTTON SENDS SERVER CALL TO DISPLAY PROFILE
    @IBAction func onSearchButton(sender: AnyObject)  
    {
        if textView.text == ""
        {
            let refreshAlert = UIAlertController(title: "Cannot Find User", message: "Empty User Name", preferredStyle: UIAlertControllerStyle.Alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(refreshAlert, animated: false, completion: nil)
        }
        
        
        else
        {
            if Reachability.isConnectedToNetwork() == true {
            let auth = defaults.objectForKey("authidentity") as! NSString
            let url:NSURL = NSURL(string: "http://54.173.40.155:3005/api/v1/user/Display_Profile/\(textView.text)")!
            let session = NSURLSession.sharedSession()
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "GET"
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            let boundaryConstant = "myRandomBoundary12345"
            let contentType = "application/json;boundary="+boundaryConstant
            request.setValue(contentType, forHTTPHeaderField: "Content-Type")
            request.setValue( "\(auth)", forHTTPHeaderField: "auth")
            print("rfdsf")
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if let httpResponse = response as? NSHTTPURLResponse {
            if(httpResponse.statusCode == 200)
                    {
                         dispatch_async(dispatch_get_main_queue())
                            {
                                self.parsingData(data!)
                             }
                    }
                        
            else  {
                  dispatch_async(dispatch_get_main_queue())
                      {
                        let refreshAlert = UIAlertController(title: "Profile Cannot Be Found", message: "Username is Invalid", preferredStyle: UIAlertControllerStyle.Alert)
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(refreshAlert, animated: false, completion: nil)
                      }
                   }
                 }
            })
            task.resume()
            }
            else
            {
                let refreshAlert = UIAlertController(title: "Internet Connection Failed", message: "Try Again Later", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self.presentViewController(refreshAlert, animated: false, completion: nil)
            }
        }
    }
    
   // FUNCTION TO PASS JSON RESPONSE FILE FROM SERVER
    func parsingData(data:NSData)
    {
        let dict: NSDictionary!=(try! NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
        let  temp = dict.valueForKey("data") as! NSDictionary
        let temp1 = temp.valueForKey("USER_PROFILE") as! NSArray
        let username1 = temp1.valueForKey("userName") as! NSArray
        let number1 = temp1.valueForKey("userPhoneNo") as! NSArray
        let email1 = temp1.valueForKey("userEmailId") as! NSArray
        let followers1 = temp1.valueForKey("no_of_Followers") as! NSArray
        let following1 = temp1.valueForKey("no_of_following") as! NSArray
        self.defaults.setObject(following1, forKey: "FOLLOWING")
        self.defaults.setObject(followers1, forKey: "FOLLOWERS")
        name.text=username1[0] as? String
        number.text=number1[0] as? String
        email.text=email1[0] as? String
        following.text = "\(following1[0])"
        followers.text = "\(followers1[0])"
    }

   }
