//
//  menuController.swift
//  twitter
//
//  Created by CL-Macmini143 on 3/4/16.
//  Copyright © 2016 CL-Macmini143. All rights reserved.
//

import UIKit

class menuController: UITableViewController
{
    var defaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    // UNWIND FUNCTION
    @IBAction func unwindToMenuController(segue: UIStoryboardSegue) {}
    
    // CALL TO SEGUE TO DISPLAY PROFILE
    @IBAction func onProfile(sender: AnyObject)
    {
        performSegueWithIdentifier("toProfileController", sender: self)
    }
    
    // FUNCTION CALL TO LOGOUT API ON SERVER
    @IBAction func onLogout(sender: AnyObject)
    {
        if Reachability.isConnectedToNetwork() == true {
        let url:NSURL = NSURL(string: "http://54.173.40.155:3005/api/v1/user/logout")!
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "PUT"
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        let auth = defaults.objectForKey("authidentity") as! NSString!
        let boundaryConstant = "myRandomBoundary12345"
        let contentType = "application/json;boundary="+boundaryConstant
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        request.setValue( "\(auth)", forHTTPHeaderField: "auth")
        let task = session.dataTaskWithRequest(request, completionHandler: {[weak self]data, response, error -> Void in
        if let httpResponse = response as? NSHTTPURLResponse
        {
           if(httpResponse.statusCode == 200)
               {
               //  NSUserDefaults.standardUserDefaults().removeObjectForKey("authidentity")
                 dispatch_async(dispatch_get_main_queue())
                {
                self?.performSegueWithIdentifier("homeSegue", sender: self)}
               
                }
            else
            {
                dispatch_async(dispatch_get_main_queue()) {
                let refreshAlert = UIAlertController(title: "Sorry", message: "Try again later", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self!.presentViewController(refreshAlert, animated: false, completion: nil)}
            }}
        })
        task.resume()
        NSUserDefaults.standardUserDefaults().removeObjectForKey("authidentity")
        }
        else
        {
            let refreshAlert = UIAlertController(title: "Internet Connection Failed", message: "Try again later", preferredStyle: UIAlertControllerStyle.Alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(refreshAlert, animated: false, completion: nil)
        }
    }
}
