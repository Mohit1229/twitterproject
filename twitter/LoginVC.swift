//
//  LogedInController.swift
//  twitter
//
//  Created by CL-Macmini143 on 2/26/16.
//  Copyright © 2016 CL-Macmini143. All rights reserved.
//

import UIKit


class Login: UIViewController,UITextFieldDelegate
{
   //VARIABLES
    var activeTextField : UITextField!
    var touchflag: Bool = true
    var passwordfieldflag: Bool = true
    var textfieldflag: Bool = true
    var keyboardflag : Bool = true
    var defaults = NSUserDefaults.standardUserDefaults()
  
   //OUTLETS
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var layout: NSLayoutConstraint!
    @IBOutlet var topspacing: NSLayoutConstraint!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

   
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewDidLoad()
    {
        activityIndicator.hidden=true
        activityIndicator.stopAnimating()
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name:UIKeyboardWillShowNotification, object: nil)
    }
    
    // UNWIND TO HOME SCREEN
    
    @IBAction func onBack(sender: AnyObject)
    {
         self.performSegueWithIdentifier("unwindToMenu", sender: self)
    }

    
    func keyboardWillShow(sender: NSNotification)
    {
        if let keyboardSize = (sender.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()
        {
         
         self.layout.constant =  keyboardSize.height
         self.view.layoutIfNeeded()
            
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        self.view.endEditing(true)
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        if touchflag == true
        {
            self.view.frame.origin.y += CGFloat(180)
            self.view.frame.size.height -= CGFloat(180)
        }
        touchflag = false
    }
    
    // FUNCTIONS TO LIFT TOOLBAR WHEN KEYPAD APPEARS
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        activeTextField = textField
        return true
    }

    func textFieldDidBeginEditing(textField: UITextField)
    {
        if activeTextField == username
        {
            if textfieldflag == true
            {
                self.view.frame.origin.y -= CGFloat(260)
                self.view.frame.size.height += CGFloat(260)
            }
        }
        textfieldflag = false
    }
    
    func textFieldShouldReturn( textField: UITextField) -> Bool
    {
        switch textField
        {
        case username :
            password.becomeFirstResponder()
            username.resignFirstResponder() 
        case password:
            password.resignFirstResponder()
        default:
            print ("Defualt case")
        }
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        if activeTextField == password
        {
            if passwordfieldflag == true
            {
                self.view.frame.origin.y += CGFloat(80)
                self.view.frame.size.height -= CGFloat(80)
            }
        }
        passwordfieldflag = false
        self.layout.constant = 0
        self.view.layoutIfNeeded()
    }

    
    // SERVER CALL TO LOGIN API
    @IBAction func logInButton(sender: AnyObject)
        
    {
        let usernamefield:NSString = username.text! as NSString
        let passwordfield:NSString = password.text! as NSString
        if ( usernamefield.isEqualToString("") || passwordfield.isEqualToString(""))
        {
            let alertController = UIAlertController(title: " Login Field ", message: "Please Fill Up Details", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(alertController, animated: false, completion: nil)
        }
        else
        {
            if Reachability.isConnectedToNetwork() == true {
            let request = NSMutableURLRequest(URL: NSURL(string: "http://54.173.40.155:3005/api/v1/user/login")!)
            let session = NSURLSession.sharedSession()
            request.HTTPMethod = "POST"
            let parameters = ["userEmailId": usernamefield as String,"userPassword": passwordfield as String,] as Dictionary<String, String>
            request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                if let httpResponse = response as? NSHTTPURLResponse
                {
                    
                    if(httpResponse.statusCode == 200)
                    {
                        dispatch_async(dispatch_get_main_queue())
                            {
                        let authkey = httpResponse.allHeaderFields["logintoken"]
                        self.defaults.setObject(authkey, forKey: "authidentity")
                        self.showTweets()
                        self.activityIndicator.hidden=true
                        self.activityIndicator.stopAnimating()
                            }
                        
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue())
                            {
                        let refreshAlert = UIAlertController(title: "Cannot Login", message: "Invalid emailid or password", preferredStyle: UIAlertControllerStyle.Alert)
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(refreshAlert, animated: false, completion: nil)
                        self.activityIndicator.hidden=true
                        self.activityIndicator.stopAnimating()
                            }
                    }
                }
            })
            task.resume()
            activityIndicator.hidden=false
            activityIndicator.startAnimating()
            }
            else
            {
                print("Internet connection FAILED")
                let refreshAlert = UIAlertController(title: "Internet Connection Failed", message: "Try again", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self.presentViewController(refreshAlert, animated: false, completion: nil)
                
            }
        }
    }
    
    // SHOW TWEET FUNCTION TO FETCH INTIAL TWEETS
    func showTweets()
    {
        
        let auth = defaults.objectForKey("authidentity") as! NSString
        let url:NSURL = NSURL(string: "http://54.173.40.155:3005/api/v1/Tweet/Display_Tweets/desending")!
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        let boundaryConstant = "myRandomBoundary12345"
        let contentType = "application/json;boundary="+boundaryConstant
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        request.setValue( "\(auth)", forHTTPHeaderField: "auth")
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
        if let httpResponse = response as? NSHTTPURLResponse
        {
           if(httpResponse.statusCode == 200)
           {
            self.parsingData(data!)
            dispatch_async(dispatch_get_main_queue())
                {
            self.performSegueWithIdentifier("loginSegue", sender: self)}
                }
        else
        {
            dispatch_async(dispatch_get_main_queue())
                {
                    let refreshAlert = UIAlertController(title: "Tweets cannot be Generated", message: "Please refresh", preferredStyle: UIAlertControllerStyle.Alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                    self.presentViewController(refreshAlert, animated: false, completion: nil)
            }
        }
        }})
    task.resume()
    }
    
    // FUNCTION TO PARSE JSON RESPONSE FROM SERVER
    func parsingData(data:NSData)
    {
        let dict: NSDictionary!=(try! NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
        let  temp = dict.valueForKey("data") as! NSDictionary
        let temp1 = temp.valueForKey("tweets") as! NSArray
        let tweets = temp1.valueForKey("tweet") as! NSArray
        let tweetid = temp1.valueForKey("_id") as! NSArray
        let like1 = temp1.valueForKey("likes") as! NSArray
        self.defaults.setObject(like1, forKey: "Like")
        self.defaults.setObject(tweetid, forKey: "IdOfTweets")
        let usernames = temp1.valueForKey("id") as! NSArray
        let users = usernames.valueForKey("userName") as! NSArray
        self.defaults.setObject(tweets, forKey: "tweets")
        self.defaults.setObject(users, forKey: "users")
    }
    



}

