//
//  tweetController.swift
//  twitter
//
//  Created by CL-Macmini143 on 3/1/16.
//  Copyright © 2016 CL-Macmini143. All rights reserved.
//

import UIKit

class tweetController: UIViewController,UITextViewDelegate{
   // OUTLETS
    @IBOutlet var textView: UITextView!
    @IBOutlet var label: UILabel!
    @IBOutlet var layout: NSLayoutConstraint!
    
   //VARIABLES
    var refreshAlert : UIAlertController!
    var defaults = NSUserDefaults.standardUserDefaults()

    override func viewWillAppear(animated: Bool)
    { self.navigationController?.navigationBarHidden = true }
    
    override func viewDidLoad()
    {   super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil)
        textView.delegate = self
   }
  
//COUNTING NO OF CHARACTERS IN TEXTVIEW
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool
    {
        if textView.text.characters.count < 140
        { return true}
        else
        {return false}
    }

    func textViewDidChange(textView: UITextView)
    {   let numberCharacters = textView.text.characters.count
        let string : Int  = 140 - numberCharacters
        label.text = String(string)
    }
    
// FUNCTION TO MOVE TOOLBAR UPWARDS
    
    func keyboardWillShow(sender: NSNotification)
    {
        if let keyboardSize = (sender.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()
        {
            self.layout.constant =  keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    func keyboardWillHide(sender: NSNotification)
    {
        self.layout.constant =  0
        self.view.layoutIfNeeded()
    }

// UNWIND SEGUE TO HOME SCREEN CONTROLLER
    @IBAction func onBack(sender: AnyObject)
    {
        self.performSegueWithIdentifier("unwindToHome", sender: self)
    }
    
  // ON TWEET FUNCTION SENDS SERVER CALL 
    @IBAction func onTweet(sender: AnyObject)
    {
        let tweetfield:NSString = textView.text! as NSString
        if ( tweetfield.isEqualToString("") )
        {
            let refreshAlert = UIAlertController(title: "FAILED", message: "Nothing to tweet", preferredStyle: UIAlertControllerStyle.Alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(refreshAlert, animated: false, completion: nil)
        }
    else
        {
            if Reachability.isConnectedToNetwork() == true {
           let auth = defaults.objectForKey("authidentity") as! NSString
            let url:NSURL = NSURL(string: "http://54.173.40.155:3005/api/v1/Tweet/Post_Tweet")!
            let session = NSURLSession.sharedSession()
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "POST"
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            let paramString = ["tweet": textView.text!]
            do
            {
                request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(paramString, options: NSJSONWritingOptions.init(rawValue: 0))}
            catch
            {
                print(error)
            }
            let boundaryConstant = "myRandomBoundary12345"
            let contentType = "application/json;boundary="+boundaryConstant
            request.setValue(contentType, forHTTPHeaderField: "Content-Type")
            request.setValue( "\(auth)", forHTTPHeaderField: "auth")
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if let httpResponse = response as? NSHTTPURLResponse {
                if(httpResponse.statusCode == 200)
                {
                    dispatch_async(dispatch_get_main_queue())
                        {
                    let refreshAlert = UIAlertController(title: "Tweet posted Successfully", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                    self.presentViewController(refreshAlert, animated: false, completion: nil)}
                         }
                    
            else {
                    dispatch_async(dispatch_get_main_queue())
                        {
                    let refreshAlert = UIAlertController(title: "Sorry", message: "Try Again", preferredStyle: UIAlertControllerStyle.Alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                    self.presentViewController(refreshAlert, animated: false, completion: nil)
                        }
                 }
            }
        })
        task.resume()
            }
            else
            {
                let refreshAlert = UIAlertController(title: "Internet Connection Failed", message: "Try Again", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self.presentViewController(refreshAlert, animated: false, completion: nil)
            }
         }
      }

  }
