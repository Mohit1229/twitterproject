//
//  addPersons.swift
//  twitter
//
//  Created by CL-Macmini143 on 3/1/16.
//  Copyright © 2016 CL-Macmini143. All rights reserved.
//

import UIKit


class addPersons: UIViewController,UITableViewDataSource,UITableViewDelegate
    
{
    var refreshAlert = UIAlertController()
    var defaults = NSUserDefaults.standardUserDefaults()
    var tablearray=[String]()
    var username1 = NSArray()
    
    //OUTLETS
    
    @IBOutlet var text: UITextField!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var textField: UITextField!
    @IBOutlet var nameOfUser: UILabel!
    
  
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBarHidden = true
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 40.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
   // ON SEARCH SEND SERVER CALL TO DISPLAY USER API
    @IBAction func onSearch(sender: AnyObject)
    {
        if(text.text=="")
        {    refreshAlert = UIAlertController(title: "Nothing to show", message: "Insert User Name", preferredStyle: UIAlertControllerStyle.Alert)
             refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
              self.presentViewController(refreshAlert, animated: false, completion: nil)
        }
        else
        {
            if Reachability.isConnectedToNetwork() == true {
            let userNAME=text.text! as String
            let auth = defaults.objectForKey("authidentity") as! NSString
            let url:NSURL = NSURL(string: "http://54.173.40.155:3005/api/v1/user/Display_Profile/\(userNAME)")!
            let session = NSURLSession.sharedSession()
            let request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "GET"
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            let boundaryConstant = "myRandomBoundary12345"
            let contentType = "application/json;boundary="+boundaryConstant
            request.setValue(contentType, forHTTPHeaderField: "Content-Type")
            request.setValue( "\(auth)", forHTTPHeaderField: "auth")
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if let httpResponse = response as? NSHTTPURLResponse
            {
            if(httpResponse.statusCode == 200)
                {
               self.parsingData(data!)
               dispatch_async(dispatch_get_main_queue())
                {
               self.tableView.reloadData()
                }
            }
            else
            {
             dispatch_async(dispatch_get_main_queue())
                {
             let refreshAlert = UIAlertController(title: "Profile Cannot Be Found", message: "Username is Invalid", preferredStyle: UIAlertControllerStyle.Alert)
             refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
             self.presentViewController(refreshAlert, animated: false, completion: nil)}
                }
             }
          })
        task.resume()
            }
            else
            {
                let refreshAlert = UIAlertController(title: "Internet Connection Failed", message: "Try Again Later", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self.presentViewController(refreshAlert, animated: false, completion: nil)
            
            }
        }
    }
    
    
// ON FOLLOW SENDS SERVER CALL TO FOLLOW API
    @IBAction func FollowUnfollow(sender: AnyObject)
    {
        if Reachability.isConnectedToNetwork() == true {
            let x=sender.tag
            print(username1[x])
            let request = NSMutableURLRequest(URL: NSURL(string: "http://54.173.40.155:3005/api/v1/Tweet/Follow_Tweet")!)
            let session = NSURLSession.sharedSession()
            let parameters = [ "name": username1[x] as! String] as Dictionary<String, String>
            request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
            let auth = defaults.objectForKey("authidentity") as! NSString
            request.HTTPMethod = "PUT"
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            let boundaryConstant = "myRandomBoundary12345"
            let contentType = "application/json;boundary="+boundaryConstant
            request.setValue(contentType, forHTTPHeaderField: "Content-Type")
            request.setValue( "\(auth)", forHTTPHeaderField: "auth")
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if let httpResponse = response as? NSHTTPURLResponse
            {
                if(httpResponse.statusCode == 200)
                    {
                        dispatch_async(dispatch_get_main_queue())
                        { 
                        self.refreshAlert = UIAlertController(title: "Successfully Followed", message:nil, preferredStyle: UIAlertControllerStyle.Alert)
                        self.refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(self.refreshAlert, animated: false, completion: nil)
                        }
                    }
                else
                   {
                        dispatch_async(dispatch_get_main_queue()) {
                        let refreshAlert = UIAlertController(title: "Profile Cannot Be Found", message: "Username is Invalid", preferredStyle: UIAlertControllerStyle.Alert)
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(refreshAlert, animated: false, completion: nil)}
                   }
                }
            })
            task.resume()
        }
            else
            {
                self.refreshAlert = UIAlertController(title: "Internet Not Available", message:"Try Again Later", preferredStyle: UIAlertControllerStyle.Alert)
                self.refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self.presentViewController(self.refreshAlert, animated: false, completion: nil)
            }
        
    }
    
   
   
    
   
    
   // UNFOLLOW SEND SERVER CALL TO UNFOLLOW API
    
    @IBAction func unFollow(sender: AnyObject)
    {
            if Reachability.isConnectedToNetwork() == true {
            let y=sender.tag
            let request = NSMutableURLRequest(URL: NSURL(string: "http://54.173.40.155:3005/api/v1/Tweet/Unfollow_Tweet")!)
            let session = NSURLSession.sharedSession()
            let parameters = [ "name": username1[y] as! String] as Dictionary<String, String>
            request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
            let auth = defaults.objectForKey("authidentity") as! NSString
            request.HTTPMethod = "PUT"
            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
            let boundaryConstant = "myRandomBoundary12345"
            let contentType = "application/json;boundary="+boundaryConstant
            request.setValue(contentType, forHTTPHeaderField: "Content-Type")
            request.setValue( "\(auth)", forHTTPHeaderField: "auth")
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if let httpResponse = response as? NSHTTPURLResponse
            {
                if(httpResponse.statusCode == 200)
                    {
                    dispatch_async(dispatch_get_main_queue())
                            {
                        self.refreshAlert = UIAlertController(title: "Successfully UnFollowed", message:nil, preferredStyle: UIAlertControllerStyle.Alert)
                        self.refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(self.refreshAlert, animated: false, completion: nil)
                            }
                    }
                        
                else
                    {
                    dispatch_async(dispatch_get_main_queue()) {
                    let refreshAlert = UIAlertController(title: "Profile Cannot Be Found", message: "Username is Invalid", preferredStyle: UIAlertControllerStyle.Alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                    self.presentViewController(refreshAlert, animated: false, completion: nil)}
                    }
                }
            })
            task.resume()
        }
        else
        {
          self.refreshAlert = UIAlertController(title: "Internet Not Available", message:"Try Again Later", preferredStyle: UIAlertControllerStyle.Alert)
          self.refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
          self.presentViewController(self.refreshAlert, animated: false, completion: nil)
        }
    }
    
    // UNWIND TO HOMESCREEN
    
    @IBAction func onBack(sender: AnyObject)
    {
        self.performSegueWithIdentifier("unwindToHome", sender: self)
        tableView.reloadData()
    }
    
    // PARSING DATA FROM JSON FILE
    func parsingData(data:NSData)
    {
        let dict: NSDictionary!=(try! NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
        let  temp = dict.valueForKey("data") as! NSDictionary
        let temp1 = temp.valueForKey("USER_PROFILE") as! NSArray
        username1 = temp1.valueForKey("userName") as! NSArray
        
    }
    
    // NO OF ROWS OF TABLEVIEW
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
      {
        return username1.count
      }
    
    // LOADING ROWS OF TABLE WITH DATA
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
     {
        let cell:FollowingClassCell = tableView.dequeueReusableCellWithIdentifier("cell") as! FollowingClassCell
        cell.nameOfUser.text=username1[indexPath.row] as? String
        cell.follow.tag=indexPath.row
        cell.unfollow.tag=indexPath.row
        return cell
    }

  
}
