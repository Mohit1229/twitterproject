//
//  loginViewController.swift
//  twitter
//
//  Created by CL-Macmini143 on 2/26/16.
//  Copyright © 2016 CL-Macmini143. All rights reserved.
//

import UIKit

class signup: UIViewController,UITextFieldDelegate
{
    // VARIABLES
    var textfieldflag: Bool = false
    var keyboardflag : Bool = false
    var refreshAlert = UIAlertController()
    var activeTextField : UITextField!
    
    //OUTLETS
    @IBOutlet var name: UITextField!
    @IBOutlet var confirmPassword: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var number: UITextField!
    @IBOutlet var layout: NSLayoutConstraint!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    // UNWIND TO HOME SCREEN
    @IBAction func onBack(sender: AnyObject)
    {
     self.performSegueWithIdentifier("unwindToMenu", sender: self)
    }
    
    
    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBarHidden = true
        self.navigationController?.viewControllers
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name:UIKeyboardWillShowNotification, object: nil)
        activityIndicator.hidden=true
        activityIndicator.stopAnimating()
    }

  
  // SEND SERVER CALL TO SIGN UP API
    @IBAction func OnNextButton(sender: AnyObject)
    {
        
        let username:NSString = name.text! as NSString
        let email:NSString = password.text! as NSString
        let userPassword:NSString = confirmPassword.text! as NSString
        let phone:NSString = number.text! as NSString
        if ( username.isEqualToString("") || email.isEqualToString("") || userPassword.isEqualToString("") || phone.isEqualToString("") )
        {
            let refreshAlert = UIAlertController(title: "SIGN UP fAILED", message: "Please fill up details ", preferredStyle: UIAlertControllerStyle.Alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(refreshAlert, animated: false, completion: nil)
                
        }
     else
        {   if Reachability.isConnectedToNetwork() == true {

            let request = NSMutableURLRequest(URL: NSURL(string: "http://54.173.40.155:3005/api/v1/user/registration")!)
            let session = NSURLSession.sharedSession()
            request.HTTPMethod = "POST"
            let parameters = [ "userName": username as String,"userPassword": userPassword as String,"userEmailId": email as String,"userPhoneNo": phone as String] as Dictionary<String, String>
            request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if let httpResponse = response as? NSHTTPURLResponse
            {
                if(httpResponse.statusCode == 200)
                {
                    dispatch_async(dispatch_get_main_queue())
                        {
                        self.performSegueWithIdentifier("NextSignUpSegue", sender: self)
                        self.activityIndicator.hidden=true
                        self.activityIndicator.stopAnimating()
                        }
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue())
                    {
                   
                    let refreshAlert = UIAlertController(title: "Sign up Failed", message: "Invalid emailid or password", preferredStyle: UIAlertControllerStyle.Alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                    self.presentViewController(refreshAlert, animated: false, completion: nil)
                    self.activityIndicator.hidden=true
                    self.activityIndicator.stopAnimating()
                        
                    }
                  
                }
            }})
            task.resume()
            activityIndicator.hidden=false
            activityIndicator.startAnimating()
        }
            else
            {
                print("Internet connection FAILED")
                print("Internet connection FAILED")
                let refreshAlert = UIAlertController(title: "Internet Connection Failed", message: "Try again", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self.presentViewController(refreshAlert, animated: false, completion: nil)
            }
        }
    }
    
   
  // FUNCTIONS TO SHIFT TOOLBAR AND TEXT FIELDS WHEN KEYPAD APPEARS
    
    func keyboardWillShow(sender: NSNotification)
    {
        if let keyboardSize = (sender.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()
         {
            self.layout.constant =  keyboardSize.height
            self.view.layoutIfNeeded()
         }
    }

   
    func textFieldDidEndEditing(textField: UITextField)
    
    {
        if activeTextField == confirmPassword
        {
        if textfieldflag == true
            {
            self.view.frame.origin.y += CGFloat(245)
            self.view.frame.size.height -= CGFloat(245)
            }
        textfieldflag = false
        }
        self.layout.constant = 0
        self.view.layoutIfNeeded()
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        self.view.endEditing(true)
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        activeTextField = textField
        return true
    }

    func textFieldDidBeginEditing(textField: UITextField)
    {
        if activeTextField == name
        {
            if textfieldflag == false
            {
             self.view.frame.origin.y -= CGFloat(245)
             self.view.frame.size.height += CGFloat(245)
            }
            textfieldflag = true
        }
    }
    
    
    func textFieldShouldReturn( textField: UITextField) -> Bool
    {
        switch textField
        {
        case name :
            number.becomeFirstResponder()
            name.resignFirstResponder()
        case number:
            password.becomeFirstResponder()
            number.resignFirstResponder()
        case password :
            confirmPassword.becomeFirstResponder()
            password.resignFirstResponder()
        case confirmPassword:
            confirmPassword.resignFirstResponder()
        default: break
        }
        return true
    }
    
  }

