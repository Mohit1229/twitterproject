//
//  FollowingClassCell.swift
//  twitter
//
//  Created by CL-Macmini143 on 3/8/16.
//  Copyright © 2016 CL-Macmini143. All rights reserved.
//

import UIKit

class FollowingClassCell: UITableViewCell
{
    @IBOutlet var nameOfUser: UILabel!
    @IBOutlet var follow: UIButton!
    @IBOutlet var unfollow: UIButton!

}
