//
//  ChangeDetails.swift
//  twitter
//
//  Created by CL-Macmini143 on 3/8/16.
//  Copyright © 2016 CL-Macmini143. All rights reserved.
//

import UIKit

class ChangeDetails: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    
    var refreshAlert = UIAlertController()
    var image:UIImage = UIImage(named:"default.jpg")!
    
    //OUTLETS
    @IBOutlet var username: UITextField!
    @IBOutlet var usernumber: UITextField!
    @IBOutlet var useremail: UITextField!
    @IBOutlet var userpassword: UITextField!
    @IBOutlet var imageView1: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        imageView1.image=image
        activityIndicator.hidden=true
        activityIndicator.stopAnimating()
    }
    
    // UNWIND TO HOME SCREEN
    
    @IBAction func onBack(sender: AnyObject)
    {
        self.performSegueWithIdentifier("unwindToMenuController", sender: self)
    }
    
    
    @IBAction func onImagePicker(sender: AnyObject)
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary)
                 {
                     let imag = UIImagePickerController()
                     imag.delegate = self
                       imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                     imag.allowsEditing = false
                       self.presentViewController(imag, animated: false, completion: nil)
                   }
 
        
    }
    
    
func imagePickerController(picker: UIImagePickerController!, var didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!)
{
    let selectedImage : UIImage = image
    imageView1.image=selectedImage
    self.dismissViewControllerAnimated(false, completion: nil)
    image=selectedImage
}


    // ON CHANGE FUNCTION SENDS SERVER CALL TO CHANGE PROFILE
    
    @IBAction func onChange(sender: AnyObject)
    {
    if ( username.text == "" || usernumber.text == "" || useremail.text == ""||userpassword.text == "" )
    {
        refreshAlert = UIAlertController(title: "Error", message: "Insert All details", preferredStyle: UIAlertControllerStyle.Alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        self.presentViewController(refreshAlert, animated: false, completion: nil)
    }
   else
    {
        if Reachability.isConnectedToNetwork() == true {
        let auth = defaults.objectForKey("authidentity") as! NSString
        let request = NSMutableURLRequest(URL: NSURL(string: "http://54.173.40.155:3005/api/v1/user/edit_Profile")!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "PUT"
        let parameters = [ "userName": username.text! as  String,"userPassword": userpassword.text! as String,"userEmailId": useremail.text! as
        String,"userPhoneNo": usernumber.text! as String] as Dictionary<String, String>
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue( "\(auth)", forHTTPHeaderField: "auth")
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
        if let httpResponse = response as? NSHTTPURLResponse {
        if(httpResponse.statusCode == 200)
            {    
                 dispatch_async(dispatch_get_main_queue()) {
                 self.performSegueWithIdentifier("unwindToMenuController", sender: self)}  }
                    
        else
            {
                 dispatch_async(dispatch_get_main_queue())
                    {
                let refreshAlert = UIAlertController(title: "Invalid Cridentials", message: "Please fill up details agaun", preferredStyle: UIAlertControllerStyle.Alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self.presentViewController(refreshAlert, animated: false, completion: nil)
                    }
            }
            }
        })
        task.resume()
        }
        else
        {
            let refreshAlert = UIAlertController(title: "Internet Connection Failed", message: "Try Again Later", preferredStyle: UIAlertControllerStyle.Alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(refreshAlert, animated: false, completion: nil)
        }
        }
    }
    
    // SERVER HIT TO UPLOAD PROFILE PIC API
    
    @IBAction func onUploadButton(sender: AnyObject)
    {
        if Reachability.isConnectedToNetwork() == true {
        let auth = defaults.objectForKey("authidentity") as! NSString
        let url:NSURL = NSURL(string: "http://54.173.40.155:3005/api/v1/user/uploadProfilePic")!
        let request = NSMutableURLRequest(URL:url)
        request.HTTPMethod = "POST"
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue( "\(auth)", forHTTPHeaderField: "auth")
        let imageData = UIImageJPEGRepresentation(imageView1.image!, 0.009)
        if(imageData==nil)  { return; }
        request.HTTPBody = createBodyWithParameters( "file",imageDataKey: imageData!, boundary: boundary)
        // myActivityIndicator.startAnimating()
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
            {
                data, response, error in
                
                if let httpResponse = response as? NSHTTPURLResponse {
                    if(httpResponse.statusCode == 200)
                    {
                        dispatch_async(dispatch_get_main_queue())
                            {
                                self.refreshAlert = UIAlertController(title: "Image Uploaded Successfully", message: nil , preferredStyle: UIAlertControllerStyle.Alert)
                                self.refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                                self.presentViewController(self.refreshAlert, animated: false, completion: nil)
                                self.activityIndicator.hidden=true
                                self.activityIndicator.stopAnimating()
                        }
                    }
                        
                    else  {
                        dispatch_async(dispatch_get_main_queue())
                            {
                                self.refreshAlert = UIAlertController(title: "Sorry", message: "Try Again Later" , preferredStyle: UIAlertControllerStyle.Alert)
                                self.refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                                self.presentViewController(self.refreshAlert, animated: false, completion: nil)
                                self.activityIndicator.hidden=true
                                self.activityIndicator.stopAnimating()
                        }
                    }
                }
        }
        task.resume()
        self.activityIndicator.hidden=false
        self.activityIndicator.startAnimating()
        }
        else
        {
            let refreshAlert = UIAlertController(title: "Internet Connection Failed", message: "Try Again Later", preferredStyle: UIAlertControllerStyle.Alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
            self.presentViewController(refreshAlert, animated: false, completion: nil)

        }
    }

    
    func createBodyWithParameters(filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData
    {
        let body = NSMutableData()
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.appendData(imageDataKey)
        body.appendString("\r\n")
        body.appendString("--\(boundary)--\r\n")
        return body
    }
 
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().UUIDString)"
    }
   }

   extension NSMutableData
   {
    func appendString(string: String)
    {
        let data = string.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        appendData(data!)
    }
}


